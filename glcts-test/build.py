#!/usr/bin/env python3

import os
import pickle
import sys
import tempfile
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from options import Options
from project_map import ProjectMap
from testers import DeqpTester, DeqpTrie, ConfigFilter
from utils.command import run_batch_command
from utils.utils import (is_soft_fp64, mesa_version, get_libdir,
                         get_libgl_drivers, get_conf_file,
                         get_blacklists, Flakes, write_disabled_tests)


# needed to preserve case in the options
# class CaseConfig(ConfigParser.SafeConfigParser):
#     def optionxform(self, optionstr):
#         return optionstr


class GLCTSLister:
    def __init__(self):
        self.pmap = ProjectMap()
        self.opt = Options()
        self.flakes = Flakes(self.pmap.current_project(), self.opt)
        self._all_tests = None

    def tests(self, env=None):
        """list tests for the suite, as a trie"""
        if self._all_tests:
            self._all_tests.seek(0)
            return pickle.load(self._all_tests)

        env = {"MESA_GLES_VERSION_OVERRIDE" : "3.2",
               "LD_LIBRARY_PATH" : get_libdir(),
               "LIBGL_DRIVERS_PATH" : get_libgl_drivers(),
               "MESA_GL_VERSION_OVERRIDE" : "4.6",
               "MESA_GLSL_VERSION_OVERRIDE" : "460"}
        self.opt.update_env(env)

        savedir = os.getcwd()
        os.chdir(self.pmap.build_root() + "/bin/gl/modules")
        run_batch_command(["./glcts", "--deqp-runmode=xml-caselist"],
                          env=env)
        all_tests = DeqpTrie()
        # Enable GL33 tests for supporting hw
        # Note: ilk, g45, etc are all < GL30 and not supported in glcts
        if self.opt.hardware in ['snb', 'ivb', 'byt']:
            all_tests.add_xml("KHR-GL33-cases.xml")
            all_tests.add_xml("GTF-GL33-cases.xml")
        else:
            all_tests.add_xml("KHR-GL46-cases.xml")
            all_tests.add_xml("GTF-GL46-cases.xml")
            all_tests.add_xml("KHR-NoContext-cases.xml")
        os.chdir(savedir)

        self._all_tests = tempfile.TemporaryFile()
        pickle.dump(all_tests, self._all_tests)
        return all_tests

    def blacklist(self, all_tests):
        """remove blacklisted tests from the trie"""
        blacklist = DeqpTrie()
        blacklist_files = get_blacklists()
        for file in blacklist_files:
            blacklist.add_txt(file)
        all_tests.filter(blacklist)

        all_tests.filter(self.flakes.test_list())
        return all_tests

    def flaky(self, _env):
        """return a trie of tests that are flaky on this platform"""
        all_tests = self.tests(_env)
        whitelist = DeqpTrie()
        for a_test in self.flakes.test_list():
            whitelist.add_line(a_test)
        all_tests.filter_whitelist(whitelist)
        blacklist = DeqpTrie()
        blacklist_files = get_blacklists()

        for file in blacklist_files:
            blacklist.add_txt(file)
        all_tests.filter(blacklist)

        return all_tests

class GLCTSTester:
    """adapts the glcts test suite to conform to the component interface"""
    def __init__(self):
        self.opt = Options()
        self.pmap = ProjectMap()

    def test(self):
        """execute all tests for the component"""
        tester = DeqpTester()
        env = {"MESA_GL_VERSION_OVERRIDE" : "4.6",
               "MESA_GLSL_VERSION_OVERRIDE" : "460",
               "MESA_SHADER_CACHE_DIR" : self.pmap.build_root()}
        if "crocus" in self.opt.hardware:
            env["MESA_LOADER_DRIVER_OVERRIDE"] = "crocus"
        binary = self.pmap.build_root() + "/bin/gl/modules/glcts"
        lister = GLCTSLister()
        results = tester.test(binary, lister, env=env)
        tester.test_flaky(binary, lister)
        lister.flakes.generate_xml()

        write_disabled_tests(self.pmap, self.opt, lister.tests(env))
        config = get_conf_file(self.opt.hardware, self.opt.arch,
                               project=self.pmap.current_project())
        tester.generate_results(results, ConfigFilter(config, self.opt))

    def build(self):
        """no build step for this test component"""

    def clean(self):
        """no clean step for this test component"""

if __name__ == '__main__':
    build(GLCTSTester())
