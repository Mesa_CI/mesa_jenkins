#!/usr/bin/env bash

dmesg | grep -qi "gpu hang" || exit 0

error_file=card_error_$1_$2_$3.txt

cat /sys/class/drm/card0/error > /tmp/$error_file

rsync /tmp/$error_file ${4/\/mnt\/jenkins/otc-mesa-ci.local::nfs}
