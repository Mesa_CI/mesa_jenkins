#!/usr/bin/env python3
import argparse
import os
import subprocess
import sys
import re

def identify_gpu_hang(clear_dmesg=False, kill_container=0):
    hang_match = re.compile(r"(.*)\[(\d*)\]$")

    if clear_dmesg:
        subprocess.call(["dmesg", "-C"])

    while(True):
        line = sys.stdin.readline()
        lowered_line = line.lower()
        if(
            "gpu hang" not in lowered_line and
            "*error* ring create req" not in lowered_line and
            "unable to purge gpu memory due lock contention" not in lowered_line and
            "i915_gem_userptr" not in lowered_line and
            "failed to idle engines, declaring wedged" not in lowered_line and
            "resetting rcs0 for hang" not in lowered_line and
            "xe device coredump has been created" not in lowered_line and
            "call trace:" not in lowered_line and
            "timedout job: seqno=" not in lowered_line
        ):
            continue

        if kill_container:
            subprocess.call(["docker", "kill", str(kill_container)])

        print("Found GPU Hang: " + line)
        found = hang_match.match(line)
        if not found:
            print("ERROR: could not find PID in string")
            continue
        msg = found.group(1)
        proc = found.group(2)
        if not os.path.exists("/proc/" + proc + "/cmdline"):
            print("Unable to determine hanging process, path doesn't exist: "
                  + "/proc/" + proc + "/cmdline")
            continue
        cmdline = open("/proc/" + proc + "/cmdline").read()
        print("Hanging process: " + cmdline.replace("\0", " "))
    
        module = None
        for token in msg.split():
            if "glcts" in msg:
                module = "glcts"
            if "deqp" not in msg:
                continue
            module = token.split("-")[-1]
            if module == "vk":
                module = "vulkan"
        if not module:
            print("Error: could not determine module from process name: " + msg)

        for arg in cmdline.split("\0"):
            if "qpa" not in arg:
                continue
            qpa = arg.split("=")[-1]
            qpa_path = "/tmp/build_root/m64/opt/deqp/modules/" + module + "/" + qpa
            if module == "glcts":
                qpa_path = "/tmp/build_root/m64/bin/gl/modules/" + qpa
                current_test = "none"
                b = None
            with open(qpa_path, 'rb') as f:
                b = f.read()

            if b is None:
                print("ERROR: results file is empty: " + qpa_path)
                continue

            try:
                lines = b.decode()
            except UnicodeDecodeError:
                lines = b.decode("ISO-8859-1")

            for qpaline in lines.split('\n'):
                if qpaline.startswith("#beginTestCaseResult"):
                    current_test = qpaline.split(" ")[-1]
                if qpaline.startswith("#endTestCaseResult"):
                    current_test = "none"

            print("Hanging test: " + current_test)

if __name__== "__main__":
    
    parser = argparse.ArgumentParser()

    parser.add_argument("--clear_dmesg", default=False, action=argparse.BooleanOptionalAction, help="NB: must run script as root")
    parser.add_argument("--kill_container", nargs=1)

    args = parser.parse_args()

    identify_gpu_hang(clear_dmesg=args.clear_dmesg, kill_container=args.kill_container[0])
