#!/usr/bin/python2
# encoding=utf-8
# Copyright © 2018 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Attempt to fetch build_support repository, then fetch additional
repositories.
"""

from __future__ import print_function
import argparse
import git
import importlib
import os
import shutil
import sys
import time
import tempfile

def repo_path(repo_name):
    ci_root = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(ci_root, "repos", repo_name)

build_support_dir = repo_path("mesa_ci")
internal_build_support_dir = repo_path("mesa_ci_internal")

ci_projects = {
    'mesa_ci': ['git://otc-mesa-ci.local/git/mirror/mesa_ci/origin',
                'https://gitlab.freedesktop.org/Mesa_CI/mesa_ci.git',],
    'mesa_ci_internal': ['git://otc-mesa-ci.local/git/mirror/mesa_ci_internal/origin',
                         'git@github.com:intel-innersource/drivers.gpu.mesa.ci.internal.git',
                         ]
}

def safe_clone(project, project_dir):
    if os.path.exists(project_dir):
        return
    repo_dir = os.path.dirname(os.path.abspath(project_dir))
    if not os.path.exists(repo_dir):
        os.makedirs(repo_dir)
    success = False
    for url in ci_projects[project]:
        try:
            print(f'Trying to clone build support from {project}')
            git.Repo.clone_from(url, project_dir)
            break
        except git.GitCommandError as e:
            print(f"ERROR: could not clone sources: {project_dir}\n{str(e)}")

def safe_sha(project_dir):
    try:
        repo = git.Repo(project_dir)
        return repo.commit().hexsha
    except:
        return ""

safe_clone('mesa_ci', build_support_dir)
bs_sha = safe_sha(build_support_dir)
safe_clone('mesa_ci_internal', internal_build_support_dir)
internal_bs_sha = safe_sha(internal_build_support_dir)

parser = argparse.ArgumentParser(description="checks out branches and commits")
parser.add_argument('--branch', type=str, default="",
                    help="The branch to base the checkout on. (default: %(default)s)")
parser.add_argument('--project', type=str, default="",
                    help="Limit commits to repos required by the project. (default: none)")
parser.add_argument('--revspec', type=str, default="",
                    help=("XML file containing revision spec listing out "
                          "projects and revisions to fetch (default: none)"))
parser.add_argument('commits', metavar='commits', type=str, nargs='*',
                    help='commits to check out, in repo=sha format')
args = parser.parse_args()

# 'Commits' parameter is searched for mesa_ci repo, which fetch sources uses to
# check out instead of 'origin/master'
only_projects = {}
if args.commits:
    for c in args.commits:
        _repo, _sha = c.lower().split('=')
        if "mesa_ci" in _repo:
            only_projects[_repo] = _sha
            if _repo == "mesa_ci":
                git.Repo(build_support_dir).remote("origin").fetch()
                git.Repo(build_support_dir).git.checkout(_sha, force=True)

sys.path.insert(0, os.path.join(build_support_dir, "build_support"))
# defensively set sys.argv to a default string, in case an object
# attempts to instantiate Options()
sys.argv = ["fetch_sources.py"]
if only_projects:
    # Load build_support modules
    from repo_set import RepoSet
    from project_map import ProjectMap
    from options import Options
    rs = RepoSet()
    rs.fetch(only_projects)
    rs.checkout(only_projects)

    if (bs_sha != safe_sha(build_support_dir) or
        internal_bs_sha != safe_sha(internal_build_support_dir)):
        print("WARN: ci sources were updated. Reexecuting fetch_sources.py "
              "in the bootstrapped environment.")
        os.execl(sys.executable, 'python', __file__, *sys.argv[1:])
        # never returns.  Now that a correct set of specifications and
        # automation are available to this script, we must reexecute with
        # the bootstrapped automation.

from dependency_graph import DependencyGraph
from project_map import ProjectMap
from repo_set import RepoSet, RevisionSpecification
from options import Options
from export import Export

def main():
    """fetch all ci sources according to the specified command line parameters"""
    repos = RepoSet()

    revisions = {}
    project_map = ProjectMap()
    build_spec = project_map.build_spec()
    exp = Export(project_map)

    # commits overrides versions in revspec
    if args.revspec and exp.result_path_exists(args.revspec):
        td = tempfile.TemporaryDirectory()
        local_spec = f"{td.name}/revspec.xml"
        exp.copy(args.revspec, local_spec)
        for commit in RevisionSpecification.from_xml_file(local_spec).to_cmd_line_param().split():
            repo, sha = commit.lower().split('=')
            revisions[repo] = sha
    if args.commits:
        for commit in args.commits:
            repo, sha = commit.lower().split('=')
            revisions[repo] = sha
    project = args.project
    branch = args.branch

    # key: repo name
    # value: branch as specified in buildspec xml
    default_branches = {}
    if branch:
        if not project:
            project = build_spec.default_project_for_ci_branch(branch)
        default_branches = build_spec.default_branches_for_ci_branch(branch)

    deps = []
    if project:
        # only fetch sources that are required for the project
        deps = DependencyGraph(project,
                               Options(args =[sys.argv[0]]),
                               repo_set=repos,
                               require_hw_support=False).all_sources(allow_missing=True)
        # the project will not be a prerequisite of itself, but we do
        # need to get its sources.
        if project not in deps:
            deps.append(project)
        repo_names = list(revisions.keys())
        for repo in repo_names:
            if repo not in deps:
                del revisions[repo]

    # eliminate deps that do not have a source repo
    for project in deps:
        repo = build_spec.repo_name_for_project(project)
        if not repo:
            # this project has no source and can't be fetched
            continue

        if repo not in revisions:
            if repo in default_branches:
                revisions[repo] = default_branches[repo]
            else:
                revisions[repo] = build_spec.default_branch_for_repo(repo)

    # obtain any sources which were not present at invocation
    cloned_new_repo = repos.clone(revisions)

    if cloned_new_repo:
        # recreate objects based on new sources
        repos = RepoSet()

    fail = True
    for i in range(15):
        repos.fetch(revisions)
        try:
            print(f"Checking out specified commit (try {str(i+1)}/15)")
            repos.checkout(revisions)
            fail = False
        except git.GitCommandError as err:
            print("Unable to checkout specified commit, retrying in 15s..")
            print("ERROR:", err)
            time.sleep(15)
        else:
            break
    if fail:
        raise Exception("ERROR: Unable to checkout specified commit.")

    # fetch (but do not check out) all revisions that are required by
    # externals in the target projects.  They need to be in the cached
    # repo so that the component can check out the required commit
    # within its source tree.
    s_root = project_map.source_root()
    sys.path.append(s_root)
    external_revisions = {}
    for project in deps:
        # check for get_external_revisions in each build.py
        build_dir = s_root + "/" + project
        if not os.path.exists(build_dir + "/build.py"):
            continue
        try:
            build_module = importlib.import_module(project + ".build")
            build_module.get_external_revisions(external_revisions)
        except:
            continue

    # obtain any sources which were not present at invocation
    if not external_revisions.keys():
        return

    cloned_new_repo = repos.clone(external_revisions.keys())
    if cloned_new_repo:
        # recreate objects based on new sources
        repos = RepoSet()

    for project, tags in external_revisions.items():
        if type(tags) != type([]):
            repos.fetch({project : tags})
            continue
        for tag in tags:
            repos.fetch({project : tag})

if __name__ == '__main__':
    main()
