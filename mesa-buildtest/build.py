#!/usr/bin/env python3

import sys
import os
import subprocess
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from builders import MesonBuilder
from build_support import build
from export import Export
from options import Options
from project_map import ProjectMap


def main():
    pm = ProjectMap()
    sd = pm.project_source_dir(pm.current_project())

    save_dir = os.getcwd()

    global_opts = Options()

    # Autodetect valid gallium drivers in Mesa source
    gallium_drivers = set(['radeonsi', 'r300', 'r600',
                           'freedreno', 'swrast', 'v3d', 'vc4',
                           'svga', 'virgl', 'panfrost',
                           'iris', 'lima', 'zink', 'asahi', 'crocus'])

    # do not build NVK, which requires unsupported meson 1.3
    vulkan_drivers = set(['amd', 'intel', 'intel_hasvk', 'swrast'])

    if not gallium_drivers:
        raise RuntimeError("ERROR: Failed to parse available gallium "
                           "drivers from meson options")

    build_tools = [
        'drm-shim',
        'dlclose-skip',
        # remove this line until we re-enable rust
        # 'etnaviv',
        'freedreno',
        'glsl',
        'intel',
        'intel-ui',
        'lima',
        'nir',
        'nouveau',
        'asahi',
        'imagination'
    ]
    options = [
        '-Dbuild-tests=true',
        f'-Dgallium-drivers={",".join(gallium_drivers)}',
        f'-Dvulkan-drivers={",".join(vulkan_drivers)}',
        '-Dgallium-vdpau=enabled',
        '-Dgallium-xa=enabled',
        '-Dgallium-va=enabled',
        '-Dgallium-nine=true',
        '-Dgallium-opencl=standalone',
        f'-Dtools={",".join(build_tools)}',
    ]

    if global_opts.config != 'debug':
        options.extend(['-Dbuildtype=release', '-Db_ndebug=true'])
    b = MesonBuilder(extra_definitions=options, install=False)

    try:
        build(b)
    except subprocess.CalledProcessError as e:
        # build may have taken us to a place where ProjectMap doesn't work
        os.chdir(save_dir)
        Export().create_failing_test("mesa-meson-buildtest", str(e))

if __name__ == '__main__':
    main()
