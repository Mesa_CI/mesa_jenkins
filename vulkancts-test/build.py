#!/usr/bin/env python3

import copy
import multiprocessing
import os
import pickle
import sys
import tempfile
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "repos", "mesa_ci", "build_support"))
from build_support import build
from export import Export
from options import Options
from project_map import ProjectMap
from testers import DeqpTester, DeqpTrie, ConfigFilter
from utils.command import run_batch_command
from utils.fulsim import Fulsim
from utils.utils import get_conf_file, get_blacklists, Flakes, write_disabled_tests


class SlowTimeout:
    def __init__(self):
        self.timeout = 60
        self.hardware = Options().hardware

    def GetDuration(self):
        if self.hardware in ["gen9atom", "bsw"]:
            self.timeout = 90
        if "_sim" in self.hardware:
            self.timeout = 600
        return self.timeout


class VulkanTestList(object):
    def __init__(self):
        self.pm = ProjectMap()
        opt = Options()
        self.hardware = opt.hardware
        self.flakes = Flakes(self.pm.current_project(), opt)
        self._test_trie = None

    def tests(self, env):
        """provide a DeqpTrie with all tests."""
        # use a cached trie if available
        if self._test_trie:
            self._test_trie.seek(0)
            return pickle.load(self._test_trie)

        deqp_dir = os.path.dirname(self.binary())
        os.chdir(deqp_dir)
        cmd = ["./" + os.path.basename(self.binary()),
               "--deqp-runmode=xml-caselist"]
        run_batch_command(cmd, env=env)

        trie = DeqpTrie()
        trie.add_xml("dEQP-VK-cases.xml")
        os.chdir(self.pm.project_build_dir())
        mustpass_file = None
        whitelist = DeqpTrie()
        mustpass_file = self.pm.project_build_dir() + '/sim_whitelist.conf'
        mustpass_daily_file = self.pm.project_build_dir() + '/sim_whitelist_daily.conf'

        if self.hardware.endswith('_sim'):
            if os.path.exists(mustpass_file):
                print("Using mustpass for simulated platforms: "
                      + mustpass_file)
                whitelist.add_txt(mustpass_file)
            if os.path.exists(mustpass_daily_file) and Options().type == "daily":
                print("Daily build: using mustpass for simulated platforms: "
                      + mustpass_daily_file + " also")
                whitelist.add_txt(mustpass_daily_file)
        else:
            # Detect the latest mustpass file to use, and use it
            mustpass_dir = (self.pm.project_source_dir("vulkancts")
                            + "/external/vulkancts/mustpass/master/")
            if not os.path.exists(mustpass_dir):
                mustpass_dir = (self.pm.project_source_dir("vulkancts")
                                + "/external/vulkancts/mustpass/main/")
            mustpass_file = mustpass_dir + "/vk-default.txt"
            if not mustpass_file:
                print(f"Unable to find a valid whitelist/mustpass to use: {mustpass_file}")
                sys.exit(1)
            # mustpass file can be an index listing many mustpass files, or it
            # can be a single huge list of all tests.
            mustpass_file_is_index = True
            with open(mustpass_file) as f:
                for line in f:
                    if not line.rstrip().endswith('.txt'):
                        mustpass_file_is_index = False
                        break
                    else:
                        whitelist.add_txt(mustpass_dir + '/' + line.rstrip())
            if not mustpass_file_is_index:
                whitelist.add_txt(mustpass_file)

        trie.filter_whitelist(whitelist)
        del whitelist
        self._test_trie = tempfile.TemporaryFile()
        pickle.dump(trie, self._test_trie)

        # use test list to expand flaky wildcards
        flaky_tests = list(self.flakes.test_list())
        for a_test in flaky_tests:
            matches = trie.matching_tests(a_test)
            self.flakes.expand_test_group(a_test, matches)
        # flake list can never include tests in the denylist
        denylist = []
        for deny_file in get_blacklists():
            denylist += [l.rstrip() for l in open(deny_file)]
        for denied in denylist:
            self.flakes.deny_test(denied)

        return trie

    def binary(self):
        return self.pm.build_root() + "/opt/deqp/modules/vulkan/deqp-vk"

    def blacklist(self, all_tests):
        # filter tests for the platform
        blacklist = all_tests.matching_tests_for_files(get_blacklists())
        all_tests.filter(blacklist)

        all_tests.filter(self.flakes.test_list())

    def flaky(self, _env):
        """return a trie of tests that are flaky on this platform"""
        whitelist = DeqpTrie()
        for a_test in self.flakes.test_list():
            whitelist.add_line(a_test)
        return whitelist

class VulkanTester(object):
    def __init__(self, env):
        if not env:
            env = {}
        self.env = env

    def build(self):
        pass
    def clean(self):
        pass
    def test(self):
        pmap = ProjectMap()
        opts = Options()
        if opts.arch == "m64":
            icd_name = "intel_icd.x86_64.json"
            hasvk_icd_name = "intel_hasvk_icd.x86_64.json"
        elif opts.arch == "m32":
            icd_name = "intel_icd.i686.json"
            hasvk_icd_name = "intel_hasvk_icd.i686.json"
        icd_path = f"{pmap.build_root()}/share/vulkan/icd.d/{icd_name}"
        hasvk_icd_path = f"{pmap.build_root()}/share/vulkan/icd.d/{hasvk_icd_name}"
        if os.path.exists(hasvk_icd_path):
            icd_path = f"{icd_path}:{hasvk_icd_path}"

        self.env.update({"VK_ICD_FILENAMES": icd_path,
                         "ANV_ABORT_ON_DEVICE_LOSS": "true",
                         "MESA_VK_ABORT_ON_DEVICE_LOSS": "true",
                         "MESA_VK_WSI_PRESENT_MODE" : "immediate",
                         "MESA_SHADER_CACHE_DIR": pmap.build_root()})

        tester = DeqpTester()
        binary = pmap.build_root() + "/opt/deqp/modules/vulkan/deqp-vk"
        params = ["--deqp-surface-type=fbo", "--deqp-shadercache=disable"]

        lister = VulkanTestList()
        results = tester.test(binary,
                              lister,
                              params,
                              env=self.env,
                              log_mem_stats=False)
        tester.test_flaky(binary, lister, env=self.env)
        lister.flakes.generate_xml()
        config = get_conf_file(opts.hardware, opts.arch,
                               project=pmap.current_project())
        write_disabled_tests(pmap, opts, lister.tests(self.env))
        tester.generate_results(results, ConfigFilter(config, opts))

if __name__ == '__main__':
    hardware = Options().hardware
    env = {"INTEL_DEBUG":"capture-all"}
    fs = Fulsim()
    import_build = True

    if "_sim" in hardware and hardware in fs.platform_keyfile:
        if fs.is_supported():
            # sim-drm.py is invoked by Fulsim.get_env, and requires build_root
            # to be populated. To work around this, import build_root now and
            # call build with import_build=False so that the build_root is only
            # imported once
            import_build = False
            Export().import_build_root()
            env.update(fs.get_env())
        else:
            print("Unable to run simulated hardware in this environment!")
            sys.exit(1)

    build(VulkanTester(env=env), time_limit=SlowTimeout(),
          import_build=import_build)
