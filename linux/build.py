#!/usr/bin/env python3

import glob
import git
import os
import sys
import shutil
from os import path, getenv

sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..",
                          "repos", "mesa_ci", "build_support"))
from build_support import build
from export import Export
from options import Options
from project_map import ProjectMap
from repo_set import RevisionSpecification
from utils.command import run_batch_command, rmtree
from utils import utils

sys.path.append(path.join(path.dirname(path.abspath(sys.argv[0])), "..", "scripts"))
import install_linux

class LinuxBuilder:
    """builds the linux kernel"""
    def __init__(self):
        """initialize member data"""
        self.opt = Options()
        self.opt.update_env(os.environ)
        self.pmap = ProjectMap()
        self.export = Export(self.pmap)

        assert self.opt.hardware == "builder"

    def build(self):
        """build the kernel"""
        src_dir = self.pmap.project_source_dir()
        build_dir = f"{src_dir}/build"
        proj_build_dir = self.pmap.project_build_dir()
        # env vars defined by deploy_kernel jenkins job
        jenkins_label = os.getenv("INSTALL_ON")
        build_xe = os.getenv("BUILD_XE") == "1"
        install_guc = os.getenv("INSTALL_GUC") != "0"
        guc_version = os.getenv("INSTALL_GUC") if install_guc else ''
        build_num = os.getenv("BUILD_NUMBER") or "1"
        linux_rev = RevisionSpecification(project_map=self.pmap).revision("linux")
        short_rev = str(linux_rev[:len(linux_rev) // 2])
        kernel_type = "xe" if build_xe else "i915"
        template_vars = {"CONFIG_LOCALVERSION": f"{kernel_type}-{short_rev}-{build_num}"}

        if not os.path.exists(build_dir):
            os.makedirs(build_dir)

        if build_xe:
            template_vars.update({
                "CONFIG_DRM_I915": "n",
                "CONFIG_DRM_XE": "m",
                "CONFIG_DRM_XE_DISPLAY": "y",
                "CONFIG_DRM_XE_FORCE_PROBE": "*",
                "CONFIG_DRM_XE_WERROR": "y",
                "CONFIG_DRM_XE_DEBUG": "y"
            })
        else:
            template_vars.update({
                "CONFIG_DRM_I915": "m",
                "CONFIG_DRM_XE": "n",
                "CONFIG_DRM_XE_DISPLAY": "n",
                "CONFIG_DRM_XE_FORCE_PROBE": "",
                "CONFIG_DRM_XE_WERROR": "n",
                "CONFIG_DRM_XE_DEBUG": "n"
            })
        with open(f"{proj_build_dir}/default.cfg.template", "r") as ftemplate:
            with open(f"{build_dir}/.config", "w") as fconfig:
                print(ftemplate.
                      read().
                      format(**template_vars),
                      file=fconfig)

        os.chdir(src_dir)
        run_batch_command(['make', 'O=build', 'olddefconfig'])
        for _ in range(5):
            try:
                run_batch_command(['make',
                                   'O=build',
                                   '-j', str(utils.cpu_count(self.opt)),
                                   "bindeb-pkg"])
                break
            except:
                print("WARN: linux kernel build failed.  "
                      "This may be due to timing bugs in the Xe kernel driver build support.")
        for installer in glob.glob(f"{src_dir}/linux-image-*deb"):
            if "-dbg_" in installer:
                continue
            shutil.copy(installer, f"{self.pmap.build_root()}/linux-image.deb")

        self.export.export()

        if jenkins_label and (jenkins_label != "None"):
            print(f"Installing linux kernel on machines with label : {jenkins_label}")
            remote_deb_path = f"{self.export.result_path}/linux/build_root/m64/linux-image.deb"
            bs_branch = git.Repo(self.pmap.
                                 source_root()).commit().hexsha

            install_linux.trigger_builds(jenkins_label,
                                         remote_deb_path,
                                         build_support_branch=bs_branch,
                                         local=False,
                                         guc_version=guc_version)


    def clean(self):
        """clean the build directory and source repo"""
        src_dir = self.pmap.project_source_dir()
        build_dir = f"{src_dir}/build"
        rmtree(build_dir)
        utils.git_clean(src_dir)

    def test(self):
        """ test has no function for this builder """
        return

def main():
    """execute the component steps"""
    build(LinuxBuilder())

if __name__ == '__main__':
    main()
